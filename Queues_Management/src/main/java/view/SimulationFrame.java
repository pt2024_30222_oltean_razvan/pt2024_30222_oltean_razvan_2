package view;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class SimulationFrame extends JFrame{
    private JTextArea log = new JTextArea("Action log",30,60);
    private JPanel mainPanel = new JPanel();
    private JPanel movingPlatform = new JPanel(new GridLayout(1,3));
    private JScrollPane logPanel = new JScrollPane(log);
    private ImageIcon blueMan = new ImageIcon("om_albastru.png");
    private ImageIcon yellowMan = new ImageIcon("om_galben.png");
    private JLabel firstPic = new JLabel();
    private JLabel secondPic = new JLabel();
    private JLabel thirdPic = new JLabel();
    private int elapsedTime = 0;
    public SimulationFrame(){
        this.setTitle("Simulation Frame");
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setSize(640, 780);

        firstPic.setIcon(blueMan);
        secondPic.setIcon(blueMan);
        thirdPic.setIcon(blueMan);
        movingPlatform.add(firstPic);
        movingPlatform.add(secondPic);
        movingPlatform.add(thirdPic);
        movingPlatform.setBackground(Color.ORANGE);
        movingPlatform.setBorder(BorderFactory.createEmptyBorder(10,10,10,10));
        log.setEditable(false);
        log.setText("");
        log.setBackground(Color.ORANGE);

        mainPanel.add(movingPlatform);
        mainPanel.add(logPanel);
        mainPanel.setBackground(Color.RED);
        this.add(mainPanel);
        this.setContentPane(mainPanel);

        Timer timer = new Timer(1000, new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                elapsedTime += 1;
                updateStickmen(elapsedTime);
            }
        });
        timer.start();
    }
    private void updateStickmen(int elapsedTime){
        if(elapsedTime % 3 == 0){
            firstPic.setIcon(yellowMan);
            secondPic.setIcon(blueMan);
            thirdPic.setIcon(blueMan);
        }else if(elapsedTime % 3 == 1){
            firstPic.setIcon(blueMan);
            secondPic.setIcon(yellowMan);
            thirdPic.setIcon(blueMan);
        }else{
            firstPic.setIcon(blueMan);
            secondPic.setIcon(blueMan);
            thirdPic.setIcon(yellowMan);
        }
    }
    public void updateLogLine(String str){
        log.setText(log.getText() + "\n" + str);
    }
    public  void updateLog(String str){
        log.setText(log.getText() + str + ", ");
    }
}
