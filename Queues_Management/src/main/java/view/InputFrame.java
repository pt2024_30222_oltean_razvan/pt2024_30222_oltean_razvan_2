package view;

import logic.SelectionPolicy;
import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionListener;

public class InputFrame extends JFrame {
    private JPanel mainPanel = new JPanel();
    private JLabel clientsLabel = new JLabel("Number of clients:");
    private JTextField clientsField = new JTextField();
    private JLabel queuesLabel = new JLabel("Number of queues:");
    private JTextField queuesField = new JTextField();
    private JLabel timeLabel = new JLabel("Simulation time:");
    private JTextField timeField = new JTextField();
    private JPanel firstPanel = new JPanel(new GridLayout(3,2));
    private JPanel secondPanel = new JPanel(new GridBagLayout());
    private JLabel arrLabel = new JLabel("Minimum and maximum arrival time");
    private JTextField minArr = new JTextField();
    private JTextField maxArr = new JTextField();
    private JLabel srvLabel = new JLabel("Minimum and maximum service time");
    private JTextField minSrv = new JTextField();
    private JTextField maxSrv = new JTextField();
    private String[] choices = {"SHORTEST_QUEUE","SHORTEST_TIME"};
    private JComboBox<String> choiceBox = new JComboBox<>(choices);
    private JButton executeButton = new JButton("Execute");
    private void gridBagLayoutSetter(){
        GridBagConstraints cal = new GridBagConstraints();
        cal.gridx = 0;
        cal.gridy = 0;
        cal.gridwidth = 2;
        cal.gridheight = 1;
        cal.fill = GridBagConstraints.BOTH;
        cal.weightx = 1.0;
        cal.weighty = 1.0;
        cal.insets = new Insets(10, 10, 10, 10);

        GridBagConstraints cmina = new GridBagConstraints();
        cmina.gridx = 0;
        cmina.gridy = 1;
        cmina.gridwidth = 1;
        cmina.gridheight = 1;
        cmina.fill = GridBagConstraints.BOTH;
        cmina.weightx = 1.0;
        cmina.weighty = 1.0;
        cmina.insets = new Insets(0, 10, 0, 1);

        GridBagConstraints cmaxa = new GridBagConstraints();
        cmaxa.gridx = 1;
        cmaxa.gridy = 1;
        cmaxa.gridwidth = 1;
        cmaxa.gridheight = 1;
        cmaxa.fill = GridBagConstraints.BOTH;
        cmaxa.weightx = 1.0;
        cmaxa.weighty = 1.0;
        cmaxa.insets = new Insets(0, 1, 0, 10);

        GridBagConstraints csl = new GridBagConstraints();
        csl.gridx = 0;
        csl.gridy = 2;
        csl.gridwidth = 2;
        csl.gridheight = 1;
        csl.fill = GridBagConstraints.BOTH;
        csl.weightx = 1.0;
        csl.weighty = 1.0;
        csl.insets = new Insets(10, 10, 10, 10);

        GridBagConstraints cmins = new GridBagConstraints();
        cmins.gridx = 0;
        cmins.gridy = 3;
        cmins.gridwidth = 1;
        cmins.gridheight = 1;
        cmins.fill = GridBagConstraints.BOTH;
        cmins.weightx = 1.0;
        cmins.weighty = 1.0;
        cmins.insets = new Insets(0, 10, 0, 1);

        GridBagConstraints cmaxs = new GridBagConstraints();
        cmaxs.gridx = 1;
        cmaxs.gridy = 3;
        cmaxs.gridwidth = 1;
        cmaxs.gridheight = 1;
        cmaxs.fill = GridBagConstraints.BOTH;
        cmaxs.weightx = 1.0;
        cmaxs.weighty = 1.0;
        cmaxs.insets = new Insets(0, 1, 0, 10);

        GridBagConstraints ccb = new GridBagConstraints();
        ccb.gridx = 0;
        ccb.gridy = 4;
        ccb.gridwidth = 1;
        ccb.gridheight = 1;
        ccb.fill = GridBagConstraints.BOTH;
        ccb.weightx = 1.0;
        ccb.weighty = 1.0;
        ccb.insets = new Insets(10, 10, 10, 10);

        GridBagConstraints cbtn = new GridBagConstraints();
        cbtn.gridx = 1;
        cbtn.gridy = 4;
        cbtn.gridwidth = 1;
        cbtn.gridheight = 1;
        cbtn.fill = GridBagConstraints.BOTH;
        cbtn.weightx = 1.0;
        cbtn.weighty = 1.0;
        cbtn.insets = new Insets(10, 10, 10, 10);

        secondPanel.add(arrLabel,cal);
        secondPanel.add(minArr,cmina);
        secondPanel.add(maxArr,cmaxa);
        secondPanel.add(srvLabel,csl);
        secondPanel.add(minSrv,cmins);
        secondPanel.add(maxSrv,cmaxs);
        secondPanel.add(choiceBox,ccb);
        secondPanel.add(executeButton,cbtn);
    }
    public InputFrame(){
        firstPanel.add(clientsLabel);
        firstPanel.add(clientsField);
        firstPanel.add(queuesLabel);
        firstPanel.add(queuesField);
        firstPanel.add(timeLabel);
        firstPanel.add(timeField);
        firstPanel.setBorder(BorderFactory.createEmptyBorder(10,10,10,10));
        gridBagLayoutSetter();

        mainPanel.add(firstPanel);
        mainPanel.add(secondPanel);
        mainPanel.setLayout(new BoxLayout(mainPanel,BoxLayout.Y_AXIS));
        this.add(mainPanel);
        this.setContentPane(mainPanel);
        this.setTitle("Queues Management - Oltean Razvan Marian");
        this.setDefaultCloseOperation(EXIT_ON_CLOSE);
        this.setSize(280,280);

        firstPanel.setBackground(new Color(0xa1,0x15,0x27));
        secondPanel.setBackground(Color.RED);
        clientsField.setBackground(Color.LIGHT_GRAY);
        queuesField.setBackground(Color.LIGHT_GRAY);
        timeField.setBackground(Color.LIGHT_GRAY);
        minArr.setBackground(Color.LIGHT_GRAY);
        maxArr.setBackground(Color.LIGHT_GRAY);
        minSrv.setBackground(Color.LIGHT_GRAY);
        maxSrv.setBackground(Color.LIGHT_GRAY);
        choiceBox.setBackground(Color.ORANGE);
        executeButton.setBackground(Color.ORANGE);


        Font labelFont = new Font("Impact", Font.PLAIN, 16);
        Font buttonFont = new Font("Impact", Font.PLAIN, 12);
        clientsLabel.setFont(labelFont);
        queuesLabel.setFont(labelFont);
        timeLabel.setFont(labelFont);
        arrLabel.setFont(labelFont);
        srvLabel.setFont(labelFont);
        executeButton.setFont(buttonFont);
    }
    public void addExecuteListener(ActionListener act){
        executeButton.addActionListener(act);
    }

    public JTextField getClientsField() {
        return clientsField;
    }

    public void setClientsField(JTextField clientsField) {
        this.clientsField = clientsField;
    }

    public JTextField getQueuesField() {
        return queuesField;
    }

    public void setQueuesField(JTextField queuesField) {
        this.queuesField = queuesField;
    }

    public JTextField getTimeField() {
        return timeField;
    }

    public void setTimeField(JTextField timeField) {
        this.timeField = timeField;
    }

    public JTextField getMinArr() {
        return minArr;
    }

    public void setMinArr(JTextField minArr) {
        this.minArr = minArr;
    }

    public JTextField getMaxArr() {
        return maxArr;
    }

    public void setMaxArr(JTextField maxArr) {
        this.maxArr = maxArr;
    }

    public JTextField getMinSrv() {
        return minSrv;
    }

    public void setMinSrv(JTextField minSrv) {
        this.minSrv = minSrv;
    }

    public JTextField getMaxSrv() {
        return maxSrv;
    }

    public void setMaxSrv(JTextField maxSrv) {
        this.maxSrv = maxSrv;
    }

    public JComboBox<String> getChoiceBox() {
        return choiceBox;
    }
    public void showError(String err){
        JOptionPane.showMessageDialog(this, err);
    }
    public void setChoiceBox(JComboBox<String> choiceBox) {
        this.choiceBox = choiceBox;
    }
}
