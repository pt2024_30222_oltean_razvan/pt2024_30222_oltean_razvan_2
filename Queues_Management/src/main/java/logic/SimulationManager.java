package logic;

import model.*;
import view.*;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.FileWriter;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class SimulationManager implements Runnable{
    public int timeLimit;
    public int maxProcessingTime;
    public int minProcessingTime;
    public int minArrivalTime;
    public int maxArrivalTime;
    public int numberOfServers;
    public int numberOfClients;
    public SelectionPolicy selectionPolicy = SelectionPolicy.SHORTEST_TIME;
    private Scheduler scheduler;
    private SimulationFrame frame;
    private InputFrame view;
    private List<Task> generatedTasks = new ArrayList<Task>();
    private volatile boolean activeThread = false;
    private int peakHour;
    private double avgWaitingTime;
    private double avgServiceTime;
    public SimulationManager(InputFrame view, SimulationFrame frame){
        this.view = view;
        this.frame = frame;
        peakHour = 0;
        avgServiceTime = 0;
        avgWaitingTime = 0;
        view.addExecuteListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    timeLimit = Integer.parseInt(view.getTimeField().getText());
                    numberOfClients = Integer.parseInt(view.getClientsField().getText());
                    numberOfServers = Integer.parseInt(view.getQueuesField().getText());
                    minProcessingTime = Integer.parseInt(view.getMinSrv().getText());
                    maxProcessingTime = Integer.parseInt(view.getMaxSrv().getText());
                    minArrivalTime = Integer.parseInt(view.getMinArr().getText());
                    maxArrivalTime = Integer.parseInt(view.getMaxArr().getText());
                    if(minArrivalTime > maxArrivalTime) {
                        view.showError("Minimum arrival time should be lower than Maximum arrival time");
                        throw new NumberFormatException();
                    }
                    if(minProcessingTime > maxProcessingTime) {
                        view.showError("Minimum service time should be lower than Maximum service time");
                        throw new NumberFormatException();
                    }
                    if (view.getChoiceBox().getSelectedItem().toString().equals("SHORTEST_TIME")) {
                        selectionPolicy = SelectionPolicy.SHORTEST_TIME;
                    }else{
                        selectionPolicy = SelectionPolicy.SHORTEST_QUEUE;
                    }
                }catch (NumberFormatException | NullPointerException ex){
                    view.showError("Insert numbers in every field");
                    return;
                }
                frame.setVisible(true);
                scheduler = new Scheduler(numberOfServers, numberOfClients);
                scheduler.changeStrategy(selectionPolicy);
                generateNRandomTasks();
                activeThread = true;
            }
        });
    }
    private void generateNRandomTasks(){
        Random random = new Random();
        for(int i = 0;i < numberOfClients;i++){
            int rArr = random.nextInt(maxArrivalTime - minArrivalTime + 1) + minArrivalTime;
            int rSrv = random.nextInt(maxProcessingTime - minProcessingTime + 1) + minProcessingTime;
            Task rTask = new Task(i + 1,rArr,rSrv);
            generatedTasks.add(rTask);
        }
    }
    @Override
    public void run(){
        try(FileWriter writer = new FileWriter("log.txt", true)) {
            while (true) {
                if (activeThread)
                    break;
                else {
                    try {
                        Thread.sleep(250);
                    } catch (InterruptedException e) {
                        throw new RuntimeException(e);
                    }
                }
            }
            double avgServiceDivisor = 0;
            double avgWaitingDivisor = 0;
            int waitingHour = 0;
            int maxWaitingHour = 0;
            int currentTime = 0;
            printFileInputs(writer);
            while (currentTime < timeLimit) {
                int ok = 1;
                for (Server s : scheduler.getServers()) {
                    if (!s.getTasksQueue().isEmpty()) {
                        ok = 0;
                        break;
                    }
                }
                if (generatedTasks.isEmpty() && ok == 1) {
                    frame.updateLogLine("Tasks completed");
                    printStatistics(avgServiceDivisor, avgWaitingDivisor);
                    printFileStatistics(writer);
                    return;
                }
                List<Task> removableTasks = new ArrayList<>();
                for (Task t : generatedTasks) {
                    if (currentTime == t.getArrivalTime()) {
                        scheduler.dispatchTask(t);
                        removableTasks.add(t);
                    }
                }
                for (Task t : removableTasks) {
                    generatedTasks.remove(t);
                }
                printFrame(currentTime);
                printFileContents(writer,currentTime);
                ExecutorService exec = Executors.newFixedThreadPool(scheduler.getMaxNoServers());
                for (Server s : scheduler.getServers()) {
                    exec.submit(s);
                }
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    throw new RuntimeException(e);
                }
                exec.shutdown();
                for (Server s : scheduler.getServers()) {
                    for (Task t : s.getTasks()) {
                        avgServiceTime += t.getServiceTime();
                        avgServiceDivisor++;
                    }
                    waitingHour += s.getWaitingPeriod().get();
                    avgWaitingTime += s.getWaitingPeriod().get();
                    avgWaitingDivisor++;
                }
                if (waitingHour > maxWaitingHour) {
                    maxWaitingHour = waitingHour;
                    peakHour = currentTime;
                }
                waitingHour = 0;
                currentTime++;
            }
            frame.updateLogLine("Time expired");
            printStatistics(avgServiceDivisor, avgWaitingDivisor);
            printFileStatistics(writer);
        }catch (Exception e){
            System.err.println("An error occurred while writing to the file: " + e.getMessage());
        }
    }
    private void printStatistics(double avgServiceDivisor,double avgWaitingDivisor){
        avgServiceTime = avgServiceTime / avgServiceDivisor;
        avgWaitingTime = avgWaitingTime / avgWaitingDivisor;
        frame.updateLogLine("Peak hour: " + peakHour);
        String formattedValue;
        formattedValue = String.format("%.2f", avgWaitingTime);
        frame.updateLogLine("Average Waiting Time: " + formattedValue);
        formattedValue = String.format("%.2f", avgServiceTime);
        frame.updateLogLine("Average Service Time: " + formattedValue);
    }
    private void printFrame(int currentTime){
        frame.updateLogLine("Time: " + currentTime);
        frame.updateLogLine("Waiting line: ");
        for(Task t:generatedTasks){
            frame.updateLog(t.toString());
        }
        int i = 1;
        for(Server s: scheduler.getServers()) {
            frame.updateLogLine("Queue " + i++ + ":" + s.toString());
        }
        frame.updateLogLine("");
    }
    private void printFileContents(FileWriter writer,int currentTime){
        try{
            writer.write("Time: " + currentTime + "\n");
            writer.write("Waiting line: ");
            for(Task t:generatedTasks){
                writer.write(t.toString());
            }
            writer.write("\n");
            int i = 1;
            for(Server s: scheduler.getServers()) {
                writer.write("Queue " + i++ + ":" + s.toString() + "\n");
            }
            writer.write("\n");
        } catch (Exception e) {
            System.err.println("An error occurred while writing to the file: " + e.getMessage());
        }
    }
    private void printFileInputs(FileWriter writer){
        try {
            writer.write("-------------------------------------------------\n");
            writer.write("N = " + numberOfClients + "\n");
            writer.write("Q = " + numberOfServers + "\n");
            writer.write("T = " + timeLimit + "\n");
            writer.write("min.arrival = " + minArrivalTime + ", max.arrival = " + maxArrivalTime + "\n");
            writer.write("min.service = " + minProcessingTime + ", max.service = " + maxProcessingTime + "\n");
        } catch (Exception e) {
            System.err.println("An error occurred while writing to the file: " + e.getMessage());
        }
    }
    private void printFileStatistics(FileWriter writer){
        try{
            writer.write("Peak hour: " + peakHour + "\n");
            String formattedValue;
            formattedValue = String.format("%.2f", avgWaitingTime);
            writer.write("Average Waiting Time: " + formattedValue + "\n");
            formattedValue = String.format("%.2f", avgServiceTime);
            writer.write("Average Service Time: " + formattedValue + "\n");
            writer.write("-------------------------------------------------" + "\n");
        } catch (Exception e) {
            System.err.println("An error occurred while writing to the file: " + e.getMessage());
        }
    }
    public static void main(String[] args){
        InputFrame view = new InputFrame();
        SimulationFrame frame = new SimulationFrame();
        SimulationManager gen = new SimulationManager(view,frame);
        Thread t = new Thread(gen);
        t.start();
        view.setVisible(true);
    }
}
