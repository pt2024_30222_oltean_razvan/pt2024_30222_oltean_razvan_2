package logic;

import model.Server;
import model.Task;

import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

public class ConcreteStrategyTime implements Strategy {
    @Override
    public void addTask(List<Server> servers, Task t){
        AtomicInteger minServiceTime = new AtomicInteger();
        minServiceTime.set(Integer.MAX_VALUE);
        Server aux = new Server();
        for(Server s: servers){
            if(s.getWaitingPeriod().get() < minServiceTime.get()) {
                minServiceTime.set(s.getWaitingPeriod().get());
                aux = s;
            }
        }
        aux.addTask(t);
    }
}
