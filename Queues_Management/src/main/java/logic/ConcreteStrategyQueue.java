package logic;

import model.Server;
import model.Task;

import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

public class ConcreteStrategyQueue implements Strategy {
    @Override
    public void addTask(List<Server> servers, Task t){
        int minNrElements = Integer.MAX_VALUE;
        Server aux = new Server();
        for(Server s: servers){
            if(s.getTasks().length < minNrElements) {
                minNrElements = s.getTasks().length;
                aux = s;
            }
        }
        aux.addTask(t);
    }
}