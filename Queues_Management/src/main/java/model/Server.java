package model;
import logic.SimulationManager;

import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.atomic.AtomicInteger;

public class Server implements Runnable{
    private BlockingQueue<Task> tasks;
    private AtomicInteger waitingPeriod = new AtomicInteger();
    public Server(){}
    public Server(int maxTasks){
        tasks = new ArrayBlockingQueue<>(maxTasks);
    }
    public void addTask(Task newTask){
        tasks.add(newTask);
        waitingPeriod.set(waitingPeriod.get() + newTask.getServiceTime());
    }
    public void run() {
        Task t = tasks.peek();
        if (t != null) {
            if (t.getServiceTime() <= 1) {
                try {
                    tasks.take();
                } catch (InterruptedException e) {
                    throw new RuntimeException(e);
                }
                return;
            }
            waitingPeriod.getAndDecrement();
            t.setServiceTime(t.getServiceTime() - 1);
            try {
                Thread.currentThread().wait();
            } catch (InterruptedException e) {
                throw new RuntimeException(e);
            }
        }
    }
    public Task[] getTasks(){
        Task[] aux = new Task[tasks.size()];
        int i = 0;
        for(Task t:tasks){
            aux[i++] = t;
        }
        return aux;
    }
    public BlockingQueue<Task> getTasksQueue(){
        return tasks;
    }
    public AtomicInteger getWaitingPeriod(){
        return waitingPeriod;
    }
    public String toString(){
        StringBuilder aux = new StringBuilder();
        for(Task q: tasks){
            aux.append(q.toString()).append(", ");
        }
        return aux.toString();
    }
}
