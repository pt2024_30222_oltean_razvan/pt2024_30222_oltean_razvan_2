package model;

public class Task implements Comparable {
    private int identification;
    private int arrivalTime;
    private int serviceTime;
    public Task(int identification,int arrivalTime,int serviceTime){
        this.identification = identification;
        this.arrivalTime = arrivalTime;
        this.serviceTime = serviceTime;
    }
    public Task(){
        this.identification = 0;
        this.arrivalTime = 0;
        this.serviceTime = 0;
    }

    public int getArrivalTime() {
        return arrivalTime;
    }

    public void setArrivalTime(int arrivalTime) {
        this.arrivalTime = arrivalTime;
    }

    public int getServiceTime() {
        return serviceTime;
    }

    public void setServiceTime(int serviceTime) {
        this.serviceTime = serviceTime;
    }

    public int getIdentification() {
        return identification;
    }

    public void setIdentification(int identification) {
        this.identification = identification;
    }
    @Override
    public int compareTo(Object o) {
        Task aux = (Task) o;
        return this.arrivalTime - aux.getArrivalTime();
    }
    public String toString(){
        return "(" + this.identification + ", " + this.arrivalTime + ", " + this.serviceTime + ")";
    }
}
